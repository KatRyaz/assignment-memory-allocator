#!/usr/bin/python3
import sys
import subprocess as sp
from typing import final
from termcolor import colored
import os

lib_dirs = 'libs'

def before_tests():
    # build with make
    sp.run( [ 'make' ], check=True, stdout=sp.PIPE ).check_returncode()

def after_tests():
    # clean with make
    sp.run( [ 'make', 'clean' ], check=True, stdout=sp.PIPE ).check_returncode()

def do_tests( tests_folder ):
    tests = os.listdir( tests_folder )
    for test in tests:
        sp.run( [ 'timeout', '5', f'./{tests_folder}/{test}' ], check=True, stdout=sp.PIPE ).check_returncode()

def main( argc, argv ):
    try:
        before_tests()
        do_tests( "tests" )
    except sp.CalledProcessError:
        print( colored( 'test failed', 'red' ) )
        raise
    finally:
        after_tests()

    print( colored( 'all passed', 'green' ) )

if __name__ == '__main__':
    try:
        main( len( sys.argv ), sys.argv )
    except KeyboardInterrupt:
        sys.exit( 'successfully halted...' )